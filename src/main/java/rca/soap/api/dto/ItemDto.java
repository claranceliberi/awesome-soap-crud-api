package rca.soap.api.dto;

import rca.soap.classc.items.ItemStatus;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

public class ItemDto {
    private int id;
    private String name;
    private String itemCode;
    private ItemStatus status;
    private double price;
    private int supplier;

    public ItemDto(int id, String name, String itemCode, ItemStatus status, double price, int supplier) {
        this.id = id;
        this.name = name;
        this.itemCode = itemCode;
        this.status = status;
        this.price = price;
        this.supplier = supplier;
    }

    public ItemDto( String name, String itemCode, ItemStatus status, double price, int supplier) {
        this.name = name;
        this.itemCode = itemCode;
        this.status = status;
        this.price = price;
        this.supplier = supplier;
    }

    public ItemDto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public ItemStatus getStatus() {
        return status;
    }

    public void setStatus(ItemStatus status) {
        this.status = status;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getSupplier() {
        return supplier;
    }

    public void setSupplier(int supplier) {
        this.supplier = supplier;
    }
}
