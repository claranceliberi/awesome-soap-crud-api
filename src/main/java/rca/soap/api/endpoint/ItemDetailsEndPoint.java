package rca.soap.api.endpoint;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import rca.soap.classc.items.ItemDetails;
import rca.soap.classc.items.CreateItemDetailsRequest;
import rca.soap.classc.items.CreateItemDetailsResponse;
import rca.soap.classc.items.DeleteItemDetailsRequest;
import rca.soap.classc.items.DeleteItemDetailsResponse;
import rca.soap.classc.items.GetAllItemDetailsRequest;
import rca.soap.classc.items.GetAllItemDetailsResponse;
import rca.soap.classc.items.GetItemDetailsRequest;
import rca.soap.classc.items.GetItemDetailsResponse;
import rca.soap.classc.items.UpdateItemDetailsRequest;
import rca.soap.classc.items.UpdateItemDetailsResponse;
import rca.soap.api.bean.Item;
import rca.soap.api.repository.IItemRepository;



@Endpoint
public class ItemDetailsEndPoint {

	@Autowired
	private IItemRepository itemRepository;


	// method
	// request ---- GetItemDetailsRequest
	// response --- GetItemDetailsResponse
	@PayloadRoot(namespace = "http://soap.rca/classc/items", localPart = "GetItemDetailsRequest")
	@ResponsePayload
	public GetItemDetailsResponse findById(@RequestPayload GetItemDetailsRequest request) {

		Item item = itemRepository.findById(request.getId()).get();

		GetItemDetailsResponse itemDetailsResponse = mapItemDetails(item);
		return itemDetailsResponse;
	}

	@PayloadRoot(namespace = "http://soap.rca/classc/items", localPart = "GetAllItemDetailsRequest")
	@ResponsePayload
	public GetAllItemDetailsResponse findAll(@RequestPayload GetAllItemDetailsRequest request) {

		GetAllItemDetailsResponse allItemDetailsResponse = new GetAllItemDetailsResponse();
		List<Item> cours = itemRepository.findAll();
		for (Item item : cours) {
			GetItemDetailsResponse itemDetailsResponse = mapItemDetails(item);
			allItemDetailsResponse.getItemDetails().add(itemDetailsResponse.getItemDetails());
		}

		return allItemDetailsResponse;
	}


	@PayloadRoot(namespace = "http://soap.rca/classc/items", localPart = "CreateItemDetailsRequest")
	@ResponsePayload
	public CreateItemDetailsResponse save(@RequestPayload CreateItemDetailsRequest request) {
		itemRepository.save(new Item(request.getItemDetails().getId(),
				request.getItemDetails().getName(),
				request.getItemDetails().getItemCode(),
				request.getItemDetails().getStatus(),
				request.getItemDetails().getPrice(),
				request.getItemDetails().getSupplier()
				));
		CreateItemDetailsResponse itemDetailsResponse = new CreateItemDetailsResponse();
		itemDetailsResponse.setItemDetails(request.getItemDetails());
		itemDetailsResponse.setMessage("Created Successfully");
		return itemDetailsResponse;
	}

	@PayloadRoot(namespace = "http://soap.rca/classc/items", localPart = "UpdateItemDetailsRequest")
	@ResponsePayload
	public UpdateItemDetailsResponse update(@RequestPayload UpdateItemDetailsRequest request) {
		UpdateItemDetailsResponse itemDetailsResponse = null;
		Optional <Item> existingItem = this.itemRepository.findById(request.getItemDetails().getId());
		if(existingItem.isEmpty() || existingItem == null) {
			itemDetailsResponse = mapItemDetail(null, "Id not found");
		}
		if(existingItem.isPresent()) {

			Item _item = existingItem.get();
			_item.setName(request.getItemDetails().getName());
			_item.setItemCode(request.getItemDetails().getItemCode());
			_item.setPrice(request.getItemDetails().getPrice());
			_item.setStatus(request.getItemDetails().getStatus());
			_item.setSupplier(request.getItemDetails().getSupplier());
			itemRepository.save(_item);
			itemDetailsResponse = mapItemDetail(_item, "Updated successfully");

		}
		return itemDetailsResponse;
	}

	@PayloadRoot(namespace = "http://soap.rca/classc/items", localPart = "DeleteItemDetailsRequest")
	@ResponsePayload
	public DeleteItemDetailsResponse save(@RequestPayload DeleteItemDetailsRequest request) {

		itemRepository.deleteById(request.getId());

		DeleteItemDetailsResponse itemDetailsResponse = new DeleteItemDetailsResponse();
		itemDetailsResponse.setMessage("Deleted Successfully");
		return itemDetailsResponse;
	}

	private GetItemDetailsResponse mapItemDetails(Item item) {
		ItemDetails itemDetails = mapItem(item);

		GetItemDetailsResponse itemDetailsResponse = new GetItemDetailsResponse();


		itemDetailsResponse.setItemDetails(itemDetails);
		return itemDetailsResponse;
	}
	private UpdateItemDetailsResponse mapItemDetail(Item item, String message) {
		ItemDetails itemDetails = mapItem(item);
		UpdateItemDetailsResponse itemDetailsResponse = new UpdateItemDetailsResponse();

		itemDetailsResponse.setItemDetails(itemDetails);
		itemDetailsResponse.setMessage(message);
		return itemDetailsResponse;
	}

	private ItemDetails mapItem(Item item) {
		ItemDetails itemDetails = new ItemDetails();
		itemDetails.setItemCode(item.getItemCode());
		itemDetails.setPrice(item.getPrice());
		itemDetails.setStatus(item.getStatus());
		itemDetails.setSupplier(item.getSupplier());
		itemDetails.setId(item.getId());
		itemDetails.setName(item.getName());
		return itemDetails;
	}

}
